import UIKit

extension UIView {
    
    func addSubviews (viewList: [UIView]) {
        for view in viewList {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(view)
        }
    }
    
    func anchorSize(width: CGFloat? = nil, height: CGFloat? = nil) {
        if let widthSize = width {
            widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        }
        if let heightSize = height {
            heightAnchor.constraint(equalToConstant: heightSize).isActive = true
        }
    }
    
    func anchor(top: NSLayoutYAxisAnchor? = nil, topConstant: CGFloat = 0,
                bottom: NSLayoutYAxisAnchor? = nil, bottomConstant: CGFloat = 0,
                left: NSLayoutXAxisAnchor? = nil, leftConstant: CGFloat = 0,
                right: NSLayoutXAxisAnchor? = nil, rightConstant: CGFloat = 0) {
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: topConstant).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -rightConstant).isActive = true
        }
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: leftConstant).isActive = true
        }
    }
    
    func anchor(centerX: NSLayoutXAxisAnchor? = nil, centerY: NSLayoutYAxisAnchor? = nil) {
        if let centerX = centerX {
            centerXAnchor.constraint(equalTo: centerX).isActive = true
        }
        if let centerY = centerY {
            centerYAnchor.constraint(equalTo: centerY).isActive = true
        }
    }
}
