import UIKit

extension URLSession {
    func dataTask(with url: URLRequest, resultCompletion: @escaping (Result<(Data, URLResponse), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                resultCompletion(.failure(error))
            } else if let dataResponse = data, let urlResponse = response as? HTTPURLResponse {
                if (200...299).contains(urlResponse.statusCode) {
                    resultCompletion(.success((dataResponse, urlResponse)))
                } else {
                    let httpError = NetworkError.http(urlResponse.statusCode, dataResponse)
                    resultCompletion(.failure(httpError))
                }
            } else {
                let httpError = NetworkError.noResponseData
                resultCompletion(.failure(httpError))
            }
        })
    }
}
