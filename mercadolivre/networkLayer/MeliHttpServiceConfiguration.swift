import UIKit

class MeliHttpServiceConfiguration: HttpProviderConfigurationProtocol {
    
    var baseUrl: String = Environment.getMeliBaseUrlApi()
    func headerParams() -> [String: String] {
        var headers: [String: String] = [:]
        headers["Content-Type"] = "application/json"
        return headers
    }
}
