import UIKit

protocol HttpProviderConfigurationProtocol {
    
    var baseUrl: String { get }
    func headerParams() -> [String: String]
}
