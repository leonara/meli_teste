import Foundation

typealias NetworkCompletion = (Result<(Data, URLResponse), NetworkError>) -> Void

class HttpProvider {
    
    let configurationProtocol: HttpProviderConfigurationProtocol
    
    init(configurationProtocol: HttpProviderConfigurationProtocol) {
        self.configurationProtocol = configurationProtocol
    }
    
    func request(_ request: HttpRequestProtocol, resultCompletion: @escaping NetworkCompletion) {
        
        do {
            let urlRequest = try getUrlRequest(request)
            if let url = urlRequest.url?.absoluteString {
                print("request: \(url)")
            }
            let session = URLSession.shared
            let task = session.dataTask(with: urlRequest) { result in
                switch result {
                case .success(let data, let response):
                    resultCompletion(.success((data, response)))
                case .failure(let error):
                    let errorConverter = request.errorConvert(error: error)
                    resultCompletion(.failure(errorConverter))
                }
            }
            task.resume()
        } catch {
            let errorConverter = request.errorConvert(error: error)
            resultCompletion(.failure(errorConverter))
        }
    }
    
    func getUrlRequest(_ request: HttpRequestProtocol) throws -> URLRequest {
        guard var components = URLComponents(string: configurationProtocol.baseUrl ) else {
            throw NetworkError.badUrl
        }
        components.path = request.urlPath
        
        if let parameters = request.queryParameters {
            components.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        guard let url = components.url else {
            throw NetworkError.badUrl
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue

        configurationProtocol.headerParams().forEach { urlRequest.addValue($0.value, forHTTPHeaderField: $0.key) }
        request.header?.forEach { urlRequest.addValue($0.value, forHTTPHeaderField: $0.key) }
        return urlRequest
    }
}
