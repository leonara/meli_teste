import Foundation

enum NetworkError: Error {
    case http (Int, Data)
    case unexpected (Error)
    case noResponseData
    case parseData
    case badUrl
    case notFound
    case forbidden
    case internalServerError
    case timeout
    case sessionExpired
    case notConnected
}
