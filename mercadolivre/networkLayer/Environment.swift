import Foundation

class Environment {
     
    enum BaseUrlApi: String {
        case DEV
            
        func getUrl() -> String {
            switch self {
            case .DEV:
                return "https://api.mercadolibre.com"
            }
        }
    }
    
    static func getMeliBaseUrlApi() -> String {
        guard let environmentUrlApi = UserDefaults.standard.string(forKey: "environment"),
            let baseUrl = BaseUrlApi(rawValue: environmentUrlApi) else {
            return BaseUrlApi.DEV.getUrl()
        }
        return baseUrl.getUrl()
    }
}
