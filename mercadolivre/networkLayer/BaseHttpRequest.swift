import Foundation

typealias RequestCompletion<T> = (Result<T, NetworkError>) -> Void
typealias ParseCompletion<T> = (Result<T, NetworkError>)

enum HttpRequestMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case put = "PUT"
}

protocol HttpRequestProtocol {
    var method: HttpRequestMethod { get }
    var urlPath: String { get }
    var header: [String: String]? { get }
    var queryParameters: [String: String]? { get }
    var body: Encodable? { get }
    func errorConvert(error: Error) -> NetworkError
}

extension HttpRequestProtocol {
    
    public func fech<T: Decodable> (provider: HttpProvider, completion: @escaping RequestCompletion<T>) {
        provider.request(self) { response  in
            switch response {
            case .success(let data, _):
                completion(self.parseData(data: data))
                print("Request concluído com sucesso")
            case .failure(let error):
                print("Erro no request: \(error)")
                completion(.failure(self.errorConvert(error: error)))
            }
        }
    }
    
    private func parseData<T: Decodable> (data: Data) -> ParseCompletion<T> {
        do {
            let response = try JSONDecoder().decode(T.self, from: data)
            return .success(response)
        } catch {
            return .failure(NetworkError.parseData)
        }
    }
}

class BaseHttpRequest: HttpRequestProtocol {

    var method: HttpRequestMethod {
        assertionFailure("method must be overridden")
        return .get
    }
    
    var urlPath: String {
        assertionFailure("urlPath must be overridden")
        return ""
    }
    
    var header: [String: String]?
    var queryParameters: [String: String]?
    var body: Encodable?
    
    func errorConvert(error: Error) -> NetworkError {
        if let appError = error as? NetworkError {
            switch appError {
            case let .http(code, _):
                switch code {
                case 404:
                    return .notFound
                case 401:
                    return .sessionExpired
                default:
                    return appError
                }
            default:
                return appError
            }
        } else {
            switch URLError.Code(rawValue: error._code) {
            case .notConnectedToInternet:
                return .notConnected
            case .timedOut:
                return .timeout
            default:
                return .internalServerError
            }
        }
    }
}
