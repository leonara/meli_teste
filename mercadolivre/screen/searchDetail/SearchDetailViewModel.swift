import UIKit
import RxSwift
import RxCocoa

struct SearchDetailViewData {
    let title: String
    let price: String
    let freeShipping: String
    let thumbnail: URL
}

class SearchDetailViewModel {
    
    let viewData: BehaviorSubject<SearchDetailViewData>
    private let model: ItemModel
    
    init(model: ItemModel) {
        self.model = model
        let shipping = model.shipping.freeShipping ? Localizable.Free_shipping.localize() : ""
        let price = String(format: "$ %.2f", model.price)
        let data = SearchDetailViewData(title: model.title, price: price, freeShipping: shipping, thumbnail: model.thumbnail)
        viewData = BehaviorSubject<SearchDetailViewData>(value: data)
    }
}
