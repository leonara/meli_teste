import Foundation
import UIKit
import RxSwift
import RxCocoa

class SearchDetailView: UIViewController {
    
    private var portraitConstraint: [NSLayoutConstraint] = []
    private var landscapeConstraint: [NSLayoutConstraint] = []
    private var viewModel: SearchDetailViewModel?
    private let disposeBag = DisposeBag()
    
    private let thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "imgReference")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private var titleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private var priceLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    private var buyButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.Buy.localize(), for: .normal)
        button.backgroundColor = .blue
        button.tintColor = .white
        button.layer.cornerRadius = 6
        return button
    }()
    
    static func initiate(viewModel: SearchDetailViewModel) -> SearchDetailView {
        let viewController = SearchDetailView()
        viewController.viewModel = viewModel
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = Localizable.Details.localize()
        view.backgroundColor = .white
        
        setupAutoLayout()
        setupRxSwift()
        setupConstraits()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(
            alongsideTransition: { _ in
                self.setupConstraits()
        },
            completion: { _ in }
        )
    }
}

extension SearchDetailView: UIManager {
    func setupAutoLayout() {
        view.addSubviews(viewList: [thumbnailImageView, buyButton, titleLabel, priceLabel])

        portraitConstraint = [
            thumbnailImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            thumbnailImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 60),
            thumbnailImageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -60),
            thumbnailImageView.heightAnchor.constraint(equalTo: thumbnailImageView.widthAnchor, multiplier: 1.0),
            buyButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16),
            buyButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            buyButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            buyButton.heightAnchor.constraint(equalToConstant: 50),
            titleLabel.topAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor, constant: 16),
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            priceLabel.bottomAnchor.constraint(equalTo: buyButton.topAnchor, constant: -16),
            priceLabel.centerXAnchor.constraint(equalTo: buyButton.centerXAnchor)
        ]
                
        landscapeConstraint = [
            thumbnailImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            thumbnailImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            thumbnailImageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            thumbnailImageView.widthAnchor.constraint(equalTo: thumbnailImageView.heightAnchor, multiplier: 1.0),
            buyButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16),
            buyButton.leftAnchor.constraint(equalTo: thumbnailImageView.rightAnchor, constant: 16),
            buyButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            buyButton.heightAnchor.constraint(equalToConstant: 50),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            titleLabel.leftAnchor.constraint(equalTo: thumbnailImageView.rightAnchor, constant: 16),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            priceLabel.bottomAnchor.constraint(equalTo: buyButton.topAnchor, constant: -16),
            priceLabel.centerXAnchor.constraint(equalTo: buyButton.centerXAnchor)
        ]
    }
    
    func setupConstraits() {
        if UIDevice.current.orientation.isLandscape {
            setLandscapeLayout()
        } else if UIDevice.current.orientation.isPortrait {
            setPortraitLayout()
        } else {
            if view.frame.width > view.frame.height {
                setLandscapeLayout()
            } else {
                setPortraitLayout()
            }
        }
    }
    
    func setPortraitLayout() {
        NSLayoutConstraint.deactivate(landscapeConstraint)
        NSLayoutConstraint.activate(portraitConstraint)
    }
    
    func setLandscapeLayout() {
        NSLayoutConstraint.deactivate(portraitConstraint)
        NSLayoutConstraint.activate(landscapeConstraint)
    }
    
    func setupRxSwift() {
        viewModel?.viewData
            .observeOn(MainScheduler.instance)
            .subscribe({ [weak self] data in
                if let data = data.element {
                    self?.titleLabel.text = data.title
                    self?.priceLabel.text = data.price
                    self?.thumbnailImageView.load(url: data.thumbnail)
                }
        }).disposed(by: disposeBag)
    }
}
