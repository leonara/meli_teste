import Foundation
import RxSwift
import RxCocoa

struct SearchResultCellData {
    let title: String
    let price: String
    let thumbnail: URL
}

class SearchResultCellViewModel {
    
    let viewData: BehaviorSubject<SearchResultCellData>
    let model: ItemModel
    
    init(model: ItemModel) {
        self.model = model
        let title = model.title
        let price = String(format: "$ %.2f", model.price)
        let thumbnail = model.thumbnail
        let data = SearchResultCellData(title: title,
                                        price: price,
                                        thumbnail: thumbnail)
        viewData = BehaviorSubject<SearchResultCellData>(value: data)
        
    }
}
