import Foundation
import RxSwift
import RxCocoa

enum SearchState {
    case initial
    case loading
    case loaded
    case noResult
    case retry
    case error
}

class SearchViewModel {
    
    let bussiness = SearchBussiness()
    let state = BehaviorSubject<SearchState>(value: .initial)
    let searchResultCellList: PublishSubject<[SearchResultCellViewModel]> = PublishSubject()
    public let disposeBag = DisposeBag()
    private var searchResultModel: SearchResultModel?
    
    func getResults(filter: String) {
        state.onNext(.loading)
        bussiness.getSearchResults(search: filter) { [weak self] result in
            switch result {
            case .success(let model):
                self?.searchResultModel = model
                let cellViewModelList = model.results.map { SearchResultCellViewModel(model: $0) }
                self?.searchResultCellList.onNext(cellViewModelList)
                if !model.results.isEmpty {
                    self?.state.onNext(.loaded)
                } else {
                    self?.state.onNext(.noResult)
                }
            case .failure:
                self?.state.onNext(.error)
            }
        }
    }
    
    func didSelectedCell(indexPath: IndexPath) -> SearchDetailView {
        if let item = searchResultModel?.results[indexPath.row] {
            let viewModel = SearchDetailViewModel(model: item)
            return SearchDetailView.initiate(viewModel: viewModel)
        } else {
            return SearchDetailView()
        }
    }
}
