import UIKit
import Foundation
import RxSwift
import RxCocoa

class SearchView: UIViewController {
    
    private let minimumSpaceCell: CGFloat = 6
    private let viewModel = SearchViewModel()
    private var searchResultCell = PublishSubject<[SearchResultCellViewModel]>()
    private let disposeBag = DisposeBag()

    private let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        return searchController
    }()
    
    private let stateLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy private var resultCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = minimumSpaceCell
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = .systemGroupedBackground
        collection.isHidden = true
        collection.register(SearchResultCell.self, forCellWithReuseIdentifier: "cell")
        return collection
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAutoLayout()
        setupRxSwift()
        
        view.backgroundColor = .white
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        resultCollectionView.delegate = self
        navigationItem.title = Localizable.How_can_we_help.localize()
        navigationItem.hidesSearchBarWhenScrolling = false
        if let navigation = navigationController {
            navigation.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 28)]
        }
    }
    
    func handleState(_ state: SearchState) {
        switch state {
        case .initial:
            self.stateLabel.text = "initial"
            print("initial state")
        case .loading:
            self.stateLabel.text = "loading"
            print("loading")
            resultCollectionView.isHidden = true
            searchController.searchBar.isUserInteractionEnabled = false
        case .loaded:
            self.stateLabel.text = "loaded"
            print("loaded")
            resultCollectionView.isHidden = false
            searchController.searchBar.isUserInteractionEnabled = true
        case .noResult:
            self.stateLabel.text = "noResult"
            print("noResult")
            resultCollectionView.isHidden = true
            searchController.searchBar.isUserInteractionEnabled = true
        case .retry:
            self.stateLabel.text = "retry"
            resultCollectionView.isHidden = true
            searchController.searchBar.isUserInteractionEnabled = true
            print("retry")
        case .error:
            self.stateLabel.text = "error"
            resultCollectionView.isHidden = true
            searchController.searchBar.isUserInteractionEnabled = true
            print("error")
        }
    }
    
    func setupRxSwift() {
        viewModel.state
            .observeOn(MainScheduler.instance)
            .subscribe({ [weak self] state in
                self?.handleState(state.element!)
        }).disposed(by: disposeBag)
        
        viewModel.searchResultCellList
            .observeOn(MainScheduler.instance)
            .bind(to: searchResultCell)
            .disposed(by: disposeBag)
        
        searchResultCell.bind(to: resultCollectionView.rx.items(cellIdentifier: "cell", cellType: SearchResultCell.self)) { (_, data, cell) in
            cell.viewModel = data
        }.disposed(by: disposeBag)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(
            alongsideTransition: { _ in
                self.resultCollectionView.collectionViewLayout.invalidateLayout()
        },
            completion: { _ in }
        )
    }
}

extension SearchView: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchController.dismiss(animated: true)
        if let text = searchBar.text {
            viewModel.getResults(filter: text)
        }
    }
}

extension SearchView: UIManager {
    func setupAutoLayout() {
        view.addSubviews(viewList: [stateLabel, resultCollectionView])
        stateLabel.anchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        resultCollectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                    bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                    left: view.leftAnchor,
                                    right: view.rightAnchor)
        view.layoutIfNeeded()
    }
}

extension SearchView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        
        if UIDevice.current.orientation.isLandscape {
            return CGSize(width: (width/2) - minimumSpaceCell, height: 88)
        } else if  UIDevice.current.orientation.isPortrait {
            return CGSize(width: width, height: 88)
        } else {
            if view.frame.width > view.frame.height {
                return CGSize(width: (width/2) - minimumSpaceCell, height: 88)
            } else {
                return CGSize(width: width, height: 88)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = viewModel.didSelectedCell(indexPath: indexPath)
        if let navigator = navigationController {
            navigator.pushViewController(viewController, animated: true)
        }
    }
}
