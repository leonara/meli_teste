import Foundation

enum SearchParams: String {
    case query = "q"
}

struct SearchParamsList {
    static func getParams(filter: String) -> [String: String] {
        return [SearchParams.query.rawValue: filter]
    }
}

final class SearchHttpRequest: BaseHttpRequest {

    override var method: HttpRequestMethod { return .get }
    override var urlPath: String {
        return "/sites/MLA/search"
    }
}
