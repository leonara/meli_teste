import Foundation

class SearchBussiness {
    
    private var searchRequest: SearchHttpRequest
    private var hostHttpProvider = HttpProvider(configurationProtocol: MeliHttpServiceConfiguration())
    
    init() {
        searchRequest = SearchHttpRequest()
    }
    
    func getSearchResults(search: String, completion: @escaping (Result<SearchResultModel, NetworkError>) -> Void) {
        searchRequest.queryParameters = SearchParamsList.getParams(filter: search)
        searchRequest.fech(provider: hostHttpProvider, completion: completion)
    }
}
