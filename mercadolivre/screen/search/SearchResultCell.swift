import UIKit
import RxSwift
import RxCocoa

class SearchResultCell: UICollectionViewCell {
    
    var viewModel: SearchResultCellViewModel? {
        didSet {
            setupRxSwift()
        }
    }
    private let disposeBag = DisposeBag()

    let thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    var titleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 2
        return label
    }()
    
    var priceLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupAutoLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupRxSwift() {
        viewModel?.viewData
            .observeOn(MainScheduler.instance)
            .subscribe({ [weak self] data in
                if let data = data.element {
                    self?.titleLabel.text = data.title
                    self?.priceLabel.text = data.price
                    self?.thumbnailImageView.load(url: data.thumbnail)
                }
        }).disposed(by: disposeBag)
    }
}

extension SearchResultCell: UIManager {
    
    func setupAutoLayout() {
        addSubviews(viewList: [thumbnailImageView, titleLabel, priceLabel])
        
        thumbnailImageView.anchor(top: topAnchor, topConstant: 0,
                                  bottom: bottomAnchor, bottomConstant: 0,
                                  left: leftAnchor, leftConstant: 0)
        thumbnailImageView.widthAnchor.constraint(equalTo: thumbnailImageView.heightAnchor, multiplier: 1.0).isActive = true
        
        titleLabel.anchor(top: topAnchor, topConstant: 8,
                          left: thumbnailImageView.rightAnchor, leftConstant: 8,
                          right: rightAnchor, rightConstant: 8)
        
        priceLabel.anchor(bottom: bottomAnchor, bottomConstant: 8,
                          right: rightAnchor, rightConstant: 8)
        layoutIfNeeded()
    }
}
