import Foundation

protocol UIManager {
    
    func setupAutoLayout()
}
