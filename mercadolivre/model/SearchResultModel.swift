import Foundation

struct SearchResultModel: Decodable {
    
    let paging: PaggingModel
    let results: [ItemModel]
}

struct PaggingModel: Decodable {
    
    let total: Int
    let offset: Int
    let limit: Int
}

struct ItemModel: Decodable {
    
    let idItem: String
    let title: String
    let price: Float
    let currency: String
    let shipping: ShippingModel
    let thumbnail: URL
    
    private enum CodingKeys: String, CodingKey {
        case idItem = "id"
        case title
        case price
        case currency = "currency_id"
        case shipping
        case thumbnail
    }
}

struct ShippingModel: Decodable {
    
    let freeShipping: Bool
    
    private enum CodingKeys: String, CodingKey {
        case freeShipping = "free_shipping"
    }
}
