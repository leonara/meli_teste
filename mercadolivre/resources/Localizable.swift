import Foundation

// swiftlint:disable identifier_name
enum Localizable: String {
    
    case search
    case How_can_we_help
    case Free_shipping
    case Buy
    case Details
    
    func localize() -> String {
        return rawValue.localize()
    }
}
// swiftlint:enable identifier_name
